import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirstAngularComponent } from './first-angular/first-angular.component';

@NgModule({
  declarations: [
    FirstAngularComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    FirstAngularComponent
  ]
})
export class MyFirstModuleModule { }
