import "./first-angular.component.css";

export class MyContactCard extends HTMLElement {
  /**
   * Lifecycle hook - компонент создан в DOM
   */
  connectedCallback() {
    this.innerHTML = `
    <div class="contact">
      <div>
        <div id="form" class="contact-form"></div>
        <button class="button">Сохранить и отправить</button>
      </div>
      <div id="card"></div>
    </div>`;

    this.querySelector("button").addEventListener(
     "click",
      this.setLocalInfo.bind(this)
    );



    this.render();
  }

  /**
   * Отрисовка изменяемых частей
   */
  render() {
    const contact = this.getLocalInfo();

    this.querySelector("#form").innerHTML = `
      <input type="text" name="name" placeholder="" required /> <label>Имя</label>
      <input type="text" name="nickname" placeholder="" required /> <label>Никнейм</label>
      <input type="text" name="email" placeholder="" required /> <label>Email</label>
      <input type="phone" name="phone" placeholder="" required /> <label>Телефон</label>
      <input type="text" name="direct" placeholder="" required /> <label>Направление</label>
      <textarea placeholder="" name="info" required></textarea>
      <label>Кратко о себе</label>
    `;
    this.querySelectorAll("input, textarea").forEach(
      el => (el.value = contact[el.name])
    );

    this.querySelector("#card").innerHTML = contact.isEmpty
      ? ""
      : `
        <div class="contact-card">
          <center class="contact-profile" >
            <img  class="round" align=center src='src/tI5BNTNH0O01.jpg' width="140" height="140">
          </center>
          
          <div class="contact-info-container">
            <div class="contact-info">
              <div class="material-icons">person</div>
              <div class="contact-data">${contact.name}</div>
            </div>
            <div class="contact-info">
              <div class="material-icons">sentiment_satisfied</div>
              <div class="contact-data">${contact.nickname}</div>
            </div>
            <div class="contact-info">
              <div class="material-icons">email</div>
              <div class="contact-data">${contact.email}</div>
            </div>
            <div class="contact-info">
              <div class="material-icons">phone</div>
              <div class="contact-data">${contact.phone}</div>
            </div>
            <div class="contact-info">
              <div class="material-icons">search</div>
              <div class="contact-data">${contact.direct}</div>
            </div>
            <div class="contact-data-container">
              <div class="material-icons">notes</div>
              <div class="contact-data">
              ${contact.info}
              </div>
            </div>
          </div>
        </div>
      `;
  }

  /**
   * Получение данных из локального хранилища
   */
  getLocalInfo() {
    const myContactCardInfo = localStorage.getItem("my-contact-card");
    const contact = myContactCardInfo
      ? JSON.parse(myContactCardInfo)
      : {
          name: "",
          nickname: "",
          email: "",
          phone: "",
          direct: "",
          info: ""
        };

    if (Object.keys(contact).every(k => contact[k] === "")) {
      contact.isEmpty = true;
    }
    return contact;
  }

  /**
   * Сохранение данных в локальном хранилище
   * @param {MouseEvent} event
   */
setLocalInfo(event) {
    const data = {};
    event.target.parentElement
      .querySelectorAll("input, textarea")
      .forEach(el => (data[el.name] = el.value));

    localStorage.setItem("my-contact-card", JSON.stringify(data));

    this.render();
  }
}

// определение custom element
let myContactCardTag = "my-contact-card";
try {
  customElements.define(myContactCardTag, MyContactCard);
} catch (error) {
  if (process.env.NODE_ENV === "development") {
    document.body.removeChild(document.querySelector(myContactCardTag));
    myContactCardTag = `my-contact-card__${new Date().getTime()}`;
    customElements.define(myContactCardTag, MyContactCard);
    let el = new MyContactCard();
    document.body.appendChild(el);
  }
}